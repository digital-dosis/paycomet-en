'use strict';
var currentIndex;
var swiperCards;

var currentIndexSteps;
var swiperSteps;
var swiperBusiness;
var swiperDemos;
var swiperTestimonials;
var swiperTestimonialsServices;
var swiperCardsServices;
var swiperStepsHow;
var swiperStepsHowMobile;

var lastdevice;
var lastScrollTop = 0;
var device;

var scrollSpeed = 500;

var demos_src = { 
  fullscreen: 'https://digitaldosis.com/Pre/pc/en/media/Demos_fullscreen.mp4',
  iframe: 'https://digitaldosis.com/Pre/pc/en/media/Demos_iFrame.mp4',
  embebido: 'https://digitaldosis.com/Pre/pc/en/media/Demos_embebido.mp4'
};



var checkHeader = function() {
  if ($('.js-change-header').length) {
    var trigger = $('.js-change-header').offset().top || false;
    var menu = $('.js-toggle-menu').offset().top;
    if (menu >= trigger) {
      $('.js-header-menu').removeClass('white');
    } else {
      $('.js-header-menu').addClass('white');
    }
    $('.hamburger-inner').css('transition','none');
    setTimeout(function () {
      $('.hamburger-inner').removeAttr('style');
    }, 300);
  }
}

var stickyHeader = function() {
  if (mediaQuery('mobile')) {
    var $header = $('.header');
    if (window.pageYOffset > 40 && !$('.navigation-menu').hasClass('is-visible')) {
      setTimeout(function () {
        if (window.pageYOffset > 40){
          $header.addClass('header--sticky');
        }
      }, 1000);
    } else {
      $header.removeClass('header--sticky');
    }
  }
}

var cookiesAdvise = function() {
  var visited = Cookies.get('Cookie');

  if (visited !== 'visited') {
      $('.cookies').removeClass('cookies--is-hidden');
  }

  setTimeout(function(){
    Cookies.set('Cookie', 'visited', { expires: 7 });
    $('.cookies').addClass('cookies--is-hidden');
  }, 20000);
}

var handleEvents = function() {

  $('.js-toggle-menu').on('click', function() {
    if(!$('.navigation-menu').hasClass('is-visible')){$('.header').removeClass('header--sticky')}
    $('.js-navigation').toggleClass('is-visible');
    $('.header').toggleClass('header--fixed');
    $('.js-toggle-menu').toggleClass('is-active');
  });

  $('.js-toggle-filters-blog').on('click', function() {
    $('.filters__items').toggleClass('is-visible');
  });

  $('.js-toggle-search').on('click', function() {
  	$('.search__form input').toggleClass('is-visible');
  	$('.header-blog .title').toggleClass('title--hidden-mobile');
  });
  $(window).on('scroll mousewheel DOMMouseScroll', function(e) {
    checkHeader();
  });
  $(window).on('scroll', function(e) {
    var scroll = $(window).scrollTop();
    if (scroll > lastScrollTop){
        $('.header').removeClass('header--sticky');
     } else {
        stickyHeader();
     }
     lastScrollTop = scroll;
  });
  $('.js-show-suscribeform').on('click', function() {
  	if( $(window).width() >= 1024 ){
  		$('html, body').animate({
	        scrollTop: 0
	    }, scrollSpeed);
  	}else{
  		$('.landing-cover__overlay').addClass('is-visible');
  	}
  });
  $('.js-close-suscribeform').on('click', function() {
    $('.landing-cover__overlay').removeClass('is-visible');
  });
  $('.js-dropdown-feature').on('click', function(){
  	var $itemDropdown = $(this);
    if (mediaQuery('desktop')) {
      $('.js-dropdown-feature').not($itemDropdown).removeClass('open');
      $('.js-dropdown-feature').not($itemDropdown).find('.landing-dropdowns__content').slideUp();
      $('.js-dropdown-feature').not($itemDropdown).find('.dropdowns__content').slideUp();
    }
  	$itemDropdown.toggleClass('open');
  	$itemDropdown.find('.landing-dropdowns__content').slideToggle();
    $itemDropdown.find('.dropdowns__content').slideToggle();
  });

  if (mediaQuery('desktop')) {
  	$('.arrow-down').on({
  		mouseenter: function(){
  			TweenMax.to($('.arrow-down'), .8, {y:20, ease: ease});
  		},
  		mouseleave: function(){
  			TweenMax.to($('.arrow-down'), .8, {y:0, ease: ease});
  		}
  	})
  };

  if (mediaQuery('desktop')) {
    $('.navigation__item--parent').on({
      mouseenter: function(){
        var left = $(this).offset();
        var targetW = $(this).outerWidth();
        var $submenu = $(this).find('.navigation__submenu');
        var positionLeft = -($submenu.width() / 2) + targetW / 2;
        $('.navigation__link').addClass('navigation__link--white');
        $('.header').addClass('goToBack');
        $('.overlay-menu').stop().fadeIn( 'fast' );
        //$('body').css('overflow', 'hidden');
        $(this).addClass('navigation__item--active-open');
        $submenu.css({'left': positionLeft + 'px','display': 'flex'});//.removeClass('navigation__submenu--hidden');
      },
      mouseleave: function(){
        $('.navigation__link').removeClass('navigation__link--white');
        $('.header').removeClass('goToBack');
        $('.overlay-menu').stop().fadeOut();
        //$('body').css('overflow', 'visible');
        $(this).removeClass('navigation__item--active-open');
        $(this).find('.navigation__submenu').fadeOut(0);//.addClass('navigation__submenu--hidden');
      }
    });

    $('.globe__circle:not(.globe__circle--center)').on({
      mouseenter: function(){
        var word = $(this).find('svg').data('center');
        $('.globe__circle--center').addClass('text-layer');
        $('.globe__circle--center p').html(word);
      },
      mouseleave: function(){
        $('.globe__circle--center').removeClass('text-layer');
      }
    });
  };


	$('a[href]').on('click', function(e) {
		e.preventDefault();
		var newLocation = $(this);

		if ( newLocation.attr('target') && newLocation.attr('target').indexOf('_blank') >=0 ){
			window.location = newLocation.attr('href');
			return;
		}
		if ( newLocation.attr('href').indexOf('mailto:') >=0 ){
			return;
		}
		if ( newLocation.attr('href').indexOf('#') >=0 ){
            return;
		}

		$('.js-fadein-page').addClass('page-wrapper--inactive');
		setTimeout(function(){
			window.location = newLocation.attr('href');
		}, 400);
	});

	$(window).bind("pageshow", function(event) {
	    if (event.originalEvent.persisted) {
	        window.location.reload()
	    }
	});

	$('.arrow-down').on('click', function(){
		$('html, body').animate({
	        scrollTop: $('.landing-features').offset().top
	    }, scrollSpeed);
	});

  $('.js-trigger-more').on('click', function(event){
    event.preventDefault();
    $('html, body').animate({
          scrollTop: $('.first-section').offset().top - 100
      }, scrollSpeed);
  });

  // Home
  $('.js-solution-toggle').on('click', function() {
    $(this).toggleClass('is-active').siblings('.js-solution-text').slideToggle(500);
  });

  if (mediaQuery('mobile') || mediaQuery('tablet') ) {
    $('.js-mobile-submenu').on('click', function(e){
      $(this).toggleClass('icon-up');
      var $target = $(this).parent();
      $target.toggleClass('parent-open');
      $target.find('.navigation__submenu').slideToggle();
    });

    $('.js-menu-lang').on('click', function(){
      $(this).find('.icon-menu-lang').toggleClass('icon-menu-lang--up')
      var $langSelected = $(this).attr('data-lang');
      $(this).siblings(".option-lang[data-lang!='" + $langSelected + "']").toggleClass('active-lang');
    });
  }

  //panels Demos
  if (mediaQuery('desktop')){
    $('.js-panelDemo').on('mouseenter', function(e){
      var panel = $(this).parent();
      $('.demos-slide').removeClass('active-slide');
      panel.addClass('active-slide');
      var panelData = $(this).data();
      $('.js-textPanel').removeClass('active-text');
      var textPanel = $(".js-textPanel[data-demo]")
                    .filter(function () {
                        return $(this).data("demo") == panelData.demo;
                    });
     textPanel.addClass('active-text');
     switch (panelData.demo) {
      case 'fullscreen':
        $('.control-arrow').css('left', 15+'%');
        break;
      case 'embebido':
        $('.control-arrow').css('left', 85+'%');
        break;
      default:
        $('.control-arrow').css('left', 50+'%');
      }
    });
  }

  //Developers menu
  $('.js-toggle-menu-dev').on('click', function() {
    $('.hamburger--dev').toggleClass('is-active');
    if( !$('.developers__menu').hasClass('developers__menu--open') ){
      $('.developers__menu').slideDown().addClass('developers__menu--open');
    }else{
      $('.developers__menu').slideUp().removeClass('developers__menu--open');
    }
  });
  $('.js-toggle-submenu-dev').on('click', function() {
    var submenuDev = $(this).find('.developers__submenu');
    if( !submenuDev.hasClass('developers__submenu--open') ){
      $('.developers__submenu').slideUp().removeClass('developers__submenu--open');
      submenuDev.slideDown().addClass('developers__submenu--open');
    }else{
      submenuDev.slideUp().removeClass('developers__submenu--open');
    }
  });

  //Modelos desplegable Mobile
  if (mediaQuery('mobile') || mediaQuery('tablet')){
    $('.js-toggle-modelos-info-show').on('click', function(){
      $(this).addClass('modelos__info--hidden');
      $(this).next('.modelos__content').slideDown();
    });

    $('.js-toggle-modelos-info-up').on('click', function(){
      $(this).parent('.modelos__content').prev().removeClass('modelos__info--hidden');
      $(this).parent('.modelos__content').slideUp();
    });
  }

  //Modelos desplegable Desktop
  if (mediaQuery('desktop')){
    $('.modelo').on('mouseenter', function(){
      var modelItem = $(this).parent();
      $('.modelos__item').removeClass('modelos__item--active');
      modelItem.addClass('modelos__item--active');
    });
  }

  //Steps how desplegables
  $('.how-content-item').on('click', function(){
    if(!$(this).hasClass('how-content-item--opened')){
      $('.how-content-item').next().slideUp();
      $('.how-content-item').removeClass('how-content-item--opened');
      $(this).addClass('how-content-item--opened').next().slideDown();
    }else{
      $(this).removeClass('how-content-item--opened').next().slideUp();
    }
  });

  //Cookies Advise
  $('.js-save-cookies').on('click', function(){
    Cookies.set('Cookie', 'visited', { expires: 7 });
    $('.cookies').addClass('cookies--is-hidden');
  });

  //Player Screen Demos
  $('.js-panelDemo').on('click', function(e){
    e.preventDefault();
    var selectedDemo = $(this).data("demo");
    var demo_src;
    switch( selectedDemo ){
      case 'iframe':
      demo_src = demos_src.iframe
      break;
      case 'fullscreen':
      demo_src = demos_src.fullscreen
      break;
      default:
      demo_src = demos_src.embebido
      break;
    }
    videoPlayerDemo(demo_src);
  });

  $('.js-close-screenPlayer').on('click', function(){
    $('.player__screen').html('');
    $('.player').removeClass('player--open');
  });

}

var videoPlayerDemo = function(video_src) {
  $('.player__screen').html('<video controls autoplay><source src="'+video_src+'" type="video/mp4">Your browser does not support the video tag.</video>');
  $('.player').addClass('player--open');
}

var destroySlider = function(slider, cb) {
  // TODO: mejorar
  if (slider === 'cards' && swiperCards) {
    $('.swiper-container--landing-cards').each(function() {
      this.swiper.destroy(true, true);
    });

    swiperCards.destroy(true, true);

    $('.swiper-container--landing-cards .swiper-wrapper').removeAttr('style');
    $('.swiper-container--landing-cards .swiper-slide').removeAttr('style');

    swiperCards = null;

  } else if (slider === 'steps' && swiperSteps) {
    $('.swiper-container--landing-steps').each(function() {
      this.swiper.destroy(true, true);
    });

    swiperSteps.destroy(true, true);

    $('.swiper-container--landing-steps .swiper-wrapper').removeAttr('style');
    $('.swiper-container--landing-steps .swiper-slide').removeAttr('style');

    swiperSteps = null;
  } else if (slider === 'business' && swiperBusiness) {
    $('.swiper-container--home-business').each(function() {
      this.swiper.destroy(true, true);
    });

    swiperBusiness.destroy(true, true);

    $('.swiper-container--home-business .swiper-wrapper').removeAttr('style');
    $('.swiper-container--home-business .swiper-slide').removeAttr('style');

    swiperBusiness = null;

  } else if (slider === 'demos' && swiperDemos) {
    $('.swiper-container--demos').each(function() {
      this.swiper.destroy(true, true);
    });

    swiperDemos.destroy(true, true);

    $('.swiper-container--demos .swiper-wrapper').removeAttr('style');
    $('.swiper-container--demos .swiper-slide').removeAttr('style');

    swiperDemos = null;

  } else if (slider === 'testimonials' && swiperTestimonials) {
    $('.swiper-container--testimonials').each(function() {
      this.swiper.destroy(true, true);
    });

    swiperTestimonials.destroy(true, true);

    $('.swiper-container--testimonials .swiper-wrapper').removeAttr('style');
    $('.swiper-container--testimonials .swiper-slide').removeAttr('style');

    swiperTestimonials = null;

  } else if (slider === 'cards-services' && swiperCardsServices) {
    $('.swiper-container--services-cards').each(function() {
      this.swiper.destroy(true, true);
    });

    swiperCardsServices.destroy(true, true);

    $('.swiper-container--services-cards .swiper-wrapper').removeAttr('style');
    $('.swiper-container--services-cards .swiper-slide').removeAttr('style');

    swiperCardsServices = null;

  } else if (slider === 'steps-how-mobile' && swiperStepsHowMobile) {
    $('.swiper-container--steps-how-mobile').each(function() {
      this.swiper.destroy(true, true);
    });

    swiperStepsHowMobile.destroy(true, true);

    $('.swiper-container--steps-how-mobile .swiper-container--steps-how .swiper-wrapper').removeAttr('style');
    $('.swiper-container--steps-how-mobile .swiper-container--steps-how .swiper-slide').removeAttr('style');

    swiperStepsHowMobile = null;

  }

  if (cb) {
    setTimeout(function() {
      cb();
    }, 50);
  }
}

var cardsLandingSlider = function(){

  destroySlider('cards', function() {

    if (($(window).width() <= 1023) && !swiperCards) {
      swiperCards = new Swiper('.swiper-container--landing-cards', {
        slidesPerView: 'auto',
        spaceBetween: 23,
        observer: true,
        pagination: {
          el: '.swiper-pagination--landing-cards',
          clickable: true,
        },
        on: {
          slideChange: function(){
            $('.cards__item').removeClass('cards__item--current');
            currentIndex = swiperCards.activeIndex + 1;
            $('.cards__item:nth-child('+currentIndex+')').addClass('cards__item--current');
          },
          reachEnd: function(){
            $('.cards__item').removeClass('cards__item--current');
            $('.cards__item:last-child').addClass('cards__item--current');
          },
        }
      });
    }

    
  });
}

var stepsLandingSlider = function(){

  destroySlider('steps', function() {

    if (($(window).width() <= 1023) && !swiperSteps) {

      swiperSteps = new Swiper('.swiper-container--landing-steps', {
        slidesPerView: 'auto',
        spaceBetween: 0,
        observer: true,
        centeredSlides: true,
        pagination: {
          el: '.swiper-pagination--landing-steps',
          clickable: true,
        }
      });
    }
  });
}

var businessSlider = function() {

  destroySlider('business', function() {
    var options = {
      slidesPerView: 'auto',
      spaceBetween: 0,
      observer: true,
      centeredSlides: true,
      pagination: {
        el: '.swiper-pagination--home-business',
        clickable: true,
      },
      navigation: {
       nextEl: '.swiper-button-next',
       prevEl: '.swiper-button-prev',
     }
    }

    if ($(window).width() >= 1024) {
      options.loop = true;
      options.loopedSlides = 4;
      options.centeredSlides = false;
      options.slidesOffsetBefore = $(window).width() * 0.5;
    }
    if (!swiperBusiness) {
      swiperBusiness = new Swiper('.swiper-container--home-business', options);
    }
  });
}

var demosSolutionsSlider = function(){

  destroySlider('demos', function() {

    if (($(window).width() <= 1023) && !swiperDemos) {

      swiperDemos = new Swiper('.swiper-container--demos', {
        slidesPerView: 'auto',
        spaceBetween: 12,
        observer: true,
        centeredSlides: true,
        initialSlide: 1,
        pagination: {
          el: '.swiper-pagination--demos',
          clickable: true,
        },
      });
    }
  });
}

var testimonialsSlider = function(){

  destroySlider('testimonials', function() {

    if (($(window).width() <= 1023) && !swiperTestimonials) {

      swiperTestimonials = new Swiper('.swiper-container--testimonials', {
        //slidesPerView: 'auto',
        //spaceBetween: 12,
        observer: true,
        centeredSlides: true,
        //initialSlide: 1,
        pagination: {
          el: '.swiper-pagination--testimonials',
          clickable: true,
        },
      });
    }
  });
}

var cardsServicesSlider = function(){

  destroySlider('cards-services', function() {

    if (($(window).width() <= 1023) && !swiperCardsServices) {
      swiperCardsServices = new Swiper('.swiper-container--services-cards', {
        slidesPerView: 'auto',
        spaceBetween: 23,
        observer: true,
        pagination: {
          el: '.swiper-pagination--services-cards',
          clickable: true,
        },
        on: {
          slideChange: function(){
            $('.cards__item').removeClass('cards__item--current');
            currentIndex = swiperCardsServices.activeIndex + 1;
            $('.cards__item:nth-child('+currentIndex+')').addClass('cards__item--current');
          },
          reachEnd: function(){
            $('.cards__item').removeClass('cards__item--current');
            $('.cards__item:last-child').addClass('cards__item--current');
          },
        }
      });
    }
  });
}


var stepsHowSlider = function(){

  swiperStepsHow = new Swiper('.swiper-container--steps-how-desktop', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    observer: true,
    pagination: {
      el: '.swiper-container--steps-how-desktop .swiper-pagination--steps-how',
      clickable: true,
    },
  });
}

var stepsHowMobileSlider = function(){

  destroySlider('steps-how-mobile', function() {

    if (($(window).width() <= 1023) && !swiperStepsHowMobile) {

      swiperStepsHowMobile = new Swiper('.swiper-container--steps-how-mobile', {
        slidesPerView: 'auto',
        spaceBetween: 0,
        observer: true,
        centeredSlides: true,
        pagination: {
          el: '.swiper-container--steps-how-mobile .swiper-pagination--steps-how',
          clickable: true,
        }
      });
    }
  });
}

var testimonialsServicesSlider = function(){

      swiperTestimonialsServices = new Swiper('.swiper-container--testimonials-services', {
        slidesPerView: 'auto',
        centeredSlides: true,
        spaceBetween: 0,
        pagination: {
          el: '.swiper-pagination--testimonials-services',
          clickable: true,
        },
      });
}

var saveInitDevice = function() {
	if (mediaQuery('desktop')) {
		lastdevice = 'desktop';
	}else{
		lastdevice = 'mobile';
	}
}

var checkDevice = function(){
	if (mediaQuery('desktop')) {
		device = 'desktop';
	}else{
		device = 'mobile';
	}

	if( lastdevice !== device ){
		location.reload();
		lastdevice = device;
	}
}

var setInheritStateForm = function(){
	if (mediaQuery('mobile')) {
		$('.landing-cover__overlay').css({"transform":"translateY(-100%)",opacity:0});
	}
}


jQuery(document).ready(function($) {
  var resizeTimer;

  $('.js-fadein-page').removeClass('page-wrapper--inactive');
  handleEvents();
  checkHeader();
  cardsLandingSlider();
  stepsLandingSlider();
  businessSlider();
  demosSolutionsSlider();
  testimonialsSlider();
  testimonialsServicesSlider();
  cardsServicesSlider();
  stepsHowSlider();
  stepsHowMobileSlider();
  saveInitDevice();
  cookiesAdvise();


   $(window).on('resize', function(e) {

	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {

      checkDevice();
	  cardsLandingSlider();
      stepsLandingSlider();
      businessSlider();
      demosSolutionsSlider();
      testimonialsSlider();
      testimonialsServicesSlider();
      cardsServicesSlider();
      stepsHowSlider();
      stepsHowMobileSlider();
	  }, 250);

	});
});
