
var ease = CustomEase.create('custom', 'M0,0 C0.126,0.382 0.139,0.691 0.314,0.82 0.412,0.892 0.818,1 1,1');

const globe = new Vivus('lines-globe', {
        file: 'https://digitaldosis.com/Pre/pc/en/img/circulo-360.svg',
        type: 'oneByOne',
        start: 'manual',
        duration: 175,
      });
      globe
        .stop()
        .reset();

var omnicanal = function() {
  var tweenOmnicanal = TweenMax.staggerFrom($('.omnicanal__title, .omnicanal__text'), 2, {y:60, opacity: 0, ease: ease}, 0.3);
  var omnicanalScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.omnicanal',
    triggerHook: 0.45,
  })
  .setTween(tweenOmnicanal)
  .addTo(controller);
}

var animateGlobe = function() {
  var scene = new ScrollMagic.Scene({
      triggerHook: 0.7,
      triggerElement: ".globe",
  })
  .on('start', function () {
      globe.play();
  })
  .addTo(controller);
}

var animateCircles = function() {
  var tweenCircles = TweenMax.staggerTo($('.globe__circle'), .1, { opacity: 1, className:'+=globe__circle--expanded' }, 0.15);
  var circlesScene = new ScrollMagic.Scene({
      reverse: false,
      triggerHook: 0.45,
      triggerElement: ".globe",
  })
  .setTween(tweenCircles)
  .addTo(controller);
}

jQuery(document).ready(function($) {
  if (mediaQuery('desktop')) {
    omnicanal();
    animateGlobe();
    animateCircles();
  }

});